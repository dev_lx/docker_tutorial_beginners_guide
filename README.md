# Docker_Tutorial_Beginners_Guide

Docker is simple tool to build, run and ship your application in container,The term container simply means package your software application with needed 
libraries and other dependencies all packed in one file.
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
Operating System: Any linux distribution

Docker Engine

Docker_Engine_installation_script: https://gitlab.com/dev_lx/docker_install.git



### Installing

The step will be

- Clone docker_engine_repository

    git clone https://gitlab.com/dev_lx/docker_install.git

- Run main file and select your distro it will automatically install docker

    sh main.sh

- Check docker version

    docker --version



## Resources 

The basics docker operation found in given link : https://gitlab.com/dev_lx/docker_tutorial_beginners_guide/edit/master/Docker_Practicle_Guide_With_Commands

Run docker with Graphical user interface with Portainer: https://gitlab.com/dev_lx/docker_tutorial_beginners_guide/blob/3b296aa7f5d574e9bd5afd44565d933905b58a11/Portainer

Dockerfile Guide: https://gitlab.com/dev_lx/docker_tutorial_beginners_guide/blob/master/Dockerfile_Guide

Container Orchestration (Docker-swarm): 
## Authors

* **Aliabbas Kothawala** - *Initial work* - [dev_lx] (https://gitlab.com/dev_lx)



